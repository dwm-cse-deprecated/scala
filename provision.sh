#! /bin/bash

set -ex

chkcomm="type java"

comans=`eval ${chkcomm} > /dev/null 2>&1 ; echo $?`
if [ ${comans} != 0 ]; then
  if [ -e /etc/profile.d/java.sh ]; then
    source /etc/profile.d/java.sh

    comans=`eval ${chkcomm} > /dev/null 2>&1 ; echo $?`
    if [ ${comans} != 0 ]; then
      echo "java no installed"
      exit 10
    fi

  else
    echo "java no installed"
    exit 20
  fi
fi

yum -y install tar

cd /usr/local/src
curl -L http://downloads.typesafe.com/scala/2.11.7/scala-2.11.7.tgz -o scala-2.11.7.tgz
tar zxvf scala-2.11.7.tgz
ln -s /usr/local/src/scala-2.11.7 /usr/local/scala

echo "export PATH=\$PATH:/usr/local/scala/bin" > /etc/profile.d/scala.sh
